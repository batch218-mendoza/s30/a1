// 1. Result of using MongoDB Aggregation to count the total number of fruits on sale.
// Insert your query below..


db.fruits.aggregate([
				{ $match: { onSale: true } },
				{$count: "fruitsOnSale"}
	]);




// 2. Result of using MongoDB Aggregation to count the total number of fruits with stock more than 20.
// Insert your query below...


db.fruits.aggregate([
				{ $match: { stock: {$gte: 20}}},
				{$count: "enoughtStock"}
	]);





// 3. Result of using MongoDB Aggregation to get the average price of fruits onSale per supplier
// Insert your query below... 


db.fruits.aggregate([
	{ $match: { onSale: true } },
	{$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}}
]);



// 4. Result of using MongoDB Aggregation to get the highest price of a fruit per supplier
// Insert your query below... 

db.fruits.aggregate([
		{ $match: { onSale: true } },
		{$group: {_id: "$supplier_id", max_price: {$max: "$price"}}}
	]);




// 5. Result of using MongoDB Aggregation to to get the lowest price of a fruit per supplier.
// Insert your query below... 

db.fruits.aggregate([
		{ $match: { onSale: true } },
		{$group: {_id: "$supplier_id", min_price: {$min: "$price"}}}
	]);